import fetch from "node-fetch";
import fs from "fs";

const apiUrl = "https://neal.fun/api/infinite-craft/pair?";
const saveFilename = 'items.json';

let items = [];
if (fs.existsSync(saveFilename)) {
  items = JSON.parse(fs.readFileSync(saveFilename));
  console.log(`Loaded ${items.elements.length} items from file`);
}
else {
  items = {
    elements: [
      { "text": "Water", "emoji": "💧", "discovered": false }, 
      { "text": "Fire", "emoji": "🔥", "discovered": false }, 
      { "text": "Wind", "emoji": "🌬️", "discovered": false }, 
      { "text": "Earth", "emoji": "🌍", "discovered": false }
    ],
    darkMode: false
  };
  console.log("No items file found, starting with default items");
}

const pair = (first, second) => {
  return new Promise((resolve, reject) => {

    let url = `${apiUrl}first=${first}&second=${second}`;

    fetch(url, {
      "headers": {
        "accept": "*/*",
        "accept-language": "en-GB,en;q=0.9,en-US;q=0.8,fr;q=0.7",
        "sec-ch-ua": "\"Chromium\";v=\"122\", \"Not(A:Brand\";v=\"24\", \"Microsoft Edge\";v=\"122\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "cookie": "__cf_bm=zTY3f7SYrPaui67hRO9NcZHI.QyNiShgtCqJ2Ub8EAo-1710527782-1.0.1.1-b2Dw2T.T.5UiC_E1hPQ6IyYG5lV39kJ.aegowGlBKSWiECGFBT2p2eBtdJVxD6Oy9E.7gEfpCyKzm.Eg5IkPQA; _ga=GA1.1.2119255153.1710527793; cf_clearance=JRdmeQfXhqlAX.m0eYbioJw3TxMnsz84QYA1mLJFzFA-1710527782-1.0.1.1-GOg0vDopnfemPNwtJ6cHHQ5p7h.r68DKSGo7_B5iFj_1y1ubdoPApqlYYj4LE3Rqc37TGAZPMWNKnMvTzMs6yw; FCCDCF=%5Bnull%2Cnull%2Cnull%2C%5B%22CP7g5sAP7g5sAEsACBENAsEoAP_gAEPgACiQINJD7D7FbSFCwHpzaLsAMAhHRsCAQoQAAASBAmABQAKQIAQCgkAQFASgBAACAAAAICZBIQAECAAACUAAQAAAAAAEAAAAAAAIIAAAgAEAAAAIAAACAAAAEAAIAAAAEAAAmAgAAIIACAAAhAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAQOhQD2F2K2kKFkPCmQWYAQBCijYEAhQAAAAkCBIAAgAUgQAgFIIAgAIFAAAAAAAAAQEgCQAAQABAAAIACgAAAAAAIAAAAAAAQQAAAAAIAAAAAAAAEAAAAAAAQAAAAIAABEhCAAQQAEAAAAAAAQAAAAAAAAAAABAAA%22%2C%222~2072.70.89.93.108.122.149.196.2253.2299.259.2357.311.313.323.2373.338.358.2415.415.449.2506.2526.486.494.495.2568.2571.2575.540.574.2624.609.2677.864.981.1029.1048.1051.1095.1097.1126.1205.1211.1276.1301.1344.1365.1415.1423.1449.1451.1570.1577.1598.1651.1716.1735.1753.1765.1870.1878.1889.1958~dv.%22%2C%225436681E-252A-40B7-BF52-2A11AEF5C5CB%22%5D%5D; FCNEC=%5B%5B%22AKsRol9KnH5P7xpuxULwQMggeH3ypFcCfZf2ifEXW06SzAe_xjSWF4AtTFiFtW4oovz1M8OvIM1Ok7Xvo_xQPzVY60_5doP1wRC4FNsCCOXQuXWAMssnsrGpbHm7IvHYSF-kJarIY7NIUXX_78P_llCVV2g9HO1cwA%3D%3D%22%5D%5D; __gads=ID=d2072a56949e25f6:T=1710527787:RT=1710527787:S=ALNI_MbJBlYoiGQw0-v4YyUNQG8xRiENmg; __gpi=UID=00000d471deb119f:T=1710527787:RT=1710527787:S=ALNI_MbLotPiX3UtdAwZhc8amez-8x7U1w; __eoi=ID=da3dbc3cf3a1a2ba:T=1710527787:RT=1710527787:S=AA-AfjZhRdM33F7bTENrUyWIVIMj; _ga_L7MJCSDHKV=GS1.1.1710527793.1.1.1710527799.0.0.0",
        "Referer": "https://neal.fun/infinite-craft/",
        "Referrer-Policy": "strict-origin-when-cross-origin"
      },
      "body": null,
      "method": "GET"
    }).then(response => {
      response.json().then(data => resolve(data));
    }).catch(err => {
      reject(err);
    });
  });

};

async function performPairing(count = 10) {
  for (let i = 0; i < count; i++) {
    let first = items.elements[Math.floor(Math.random() * items.elements.length)];
    let second = items.elements[Math.floor(Math.random() * items.elements.length)];

    try {
      console.log(`Pairing ${first.text} and ${second.text}`);
      let data = await pair(first, second);
      //console.log(`${data.emoji}\t${data.result}`);
      console.log(data.result);
      if (!items.elements.includes(data.result)){
        items.elements.push({
          text: data.result,
          emoji: data.emoji,
          discovered: data.isNew
        });
        fs.writeFileSync(saveFilename, JSON.stringify(items));
      }
    } catch (error) {
      console.error('Pairing error :', error);
    }

    await new Promise(resolve => setTimeout(resolve, 2000 + Math.random() * 1000));
  }
}

performPairing(3000);

